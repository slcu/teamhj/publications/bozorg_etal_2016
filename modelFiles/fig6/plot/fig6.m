clear all
M=figure 

load length3d.data;

D=length3d;

l=D(:,1);
L=D(:,2);
Le=D(:,3);
lL=D(:,4);
lLe=D(:,5);
stretch=D(:,6);
stretche=D(:,7);
strain=D(:,8);
straine=D(:,9);


hFig = figure(1);
set(hFig, 'Position', [300 800 500 800]);





subplot(2,1,1);
plot(L,Le,'k.','MarkerSize',1);
axis square;
xlim([0 0.45])
ylim([0 0.45])
xlabel('L');
ylabel('L_e');

ax=gca;
ax.XTick=[0 0.2 0.4];
ax.YTick=[0 0.2 0.4];
set(ax,'FontSize',22);


% subplot(2,2,2);
% plot(lL,lLe,'k.','MarkerSize',1);
% axis square;
% xlim([0 0.03])
% ylim([0 0.03])
% xlabel('l-L');
% ylabel('l-L_e');
% 
% subplot(2,2,3);
% plot(stretch,stretche,'k.','MarkerSize',1);
% axis square;
% xlim([0.0 0.05])
% ylim([0.0 0.05])
% xlabel('(l-L)/l');
% ylabel('(l-L_e)/l');

subplot(2,1,2);
plot(strain,straine,'k.','MarkerSize',1);
axis square;
xlim([0.0 0.11])
ylim([0.0 0.11])
xlabel('(l^2-L^2)/l^2');
ylabel('(l^2-L_e^2)/l^2');
hold off;

ax=gca;
%ax.XTick=[0.035 0.040 0.045];
%ax.YTick=[0.035 0.040 0.045];
set(ax,'FontSize',22);

 nS=size(strain);
 
 
 RMSE=sqrt(sum((strain-straine).^2/nS(1)))/mean(strain)
 % teta(i)=(teta(i)*180)/3.14;
% end
% 
% for i=1:size(s1)
% missesS(i,1)=sqrt(s1(i)*s1(i)+s2(i)*s2(i)-s1(i)*s2(i));
% end
% shift=400;
% for i=1:size(s1)
% s2(i)=s2(i)+shift;
% end
% 
% 
% 
% 
% plot(teta,s1,'r')
% hold on
% plot(teta,s2,'g')
% %plot(teta,missesS)
% 
% 
% xlim([0 180])
% ylim([520 570])
% xlabel('anisotropy direction (deg)');
% ylabel('stress (KPa)')
% 
% load fig2Dshell20.data;
% Ds=fig2Dshell20;
% 
% tetas=Ds(:,1);
% s1s=Ds(:,3);
% s2s=Ds(:,4);
% for i=1:size(s1s)
% tetas(i)=(tetas(i)*180)/3.14;
% end
% 
% for i=1:size(s1s)
% missesSs(i,1)=sqrt(s1s(i)*s1s(i)+s2s(i)*s2s(i)-s1s(i)*s2s(i));
% end
% for i=1:size(s1s)
% s2s(i)=s2s(i)+shift;
% end
% 
% 
% 
% plot(tetas,s1s,'xr')
% hold on
% plot(tetas,s2s,'xg')
% %plot(tetas,missesSs,'xb')
% legend('stress1 TRBS','stress2 TRBS','stress1 shell','stress2 shell')
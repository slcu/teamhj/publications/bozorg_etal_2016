## Introduction

This repository host the files needed to reproduce the results in the paper

Bozorg, B., Krupinski, P. and Jönsson, H. (2016) A continuous growth model for plant tissue
Physical Biology, 13 (6. 065002)https://doi.org/10.1088/1478-3975/13/6/065002

Preprint available also via: https://www.repository.cam.ac.uk/handle/1810/261543

## Software

All simulations have been done using the tissue software available via the Sainsbury 
Laboratory software repository https://gitlab.com/slcu/teamhj/tissue. Instructions 
for downloading, compiling and running simulations in general are available in the 
Documentation in the tissue repository.

The tissue software is developed to simulate plant tissues including molecular, mechanical
rules on a growing and dividing structure, see eg. [1-3]. 

In short, simulations are run within a terminal using

> TISSUE/bin/simulator model init solver > out.data

where <i>TISSUE</i> is the main directory of the download, <i>model</i> is the file 
defining the model reactions, <i>init</i> is the file specifying the geometry and 
initial state of variables, and <i>solver</i> is a file setting parameters for the 
numerical solver to be used for the simulation. <i>out.data</i> is the file where 
specific output data is stored.

In many simulations, the output is stored in a directory <i>vtk</i>, and can be 
visualised using e.g. [Paraview](https://www.paraview.org).


## Generating the results and figures

In the directory 'modelFiles' you can find all the model/init/solver and plot-files needed
for generating the result presented in the paper. See the Readme in the 'modelFiles'
folder (and sub folders) for detailed instructions.

## Contact

Main contact is henrik.jonsson@slcu.cam.ac.uk.

## References

[1] Developmental patterning by mechanical signals in Arabidopsis. O. Hamant, M.
Heisler, H. Jonsson, P. Krupinski, M. Uyttewaal, P. Bokov, F. Corson, P. Sahlin, A.
Boudaoud, E. M. Meyerowitz, Y. Couder, and J. Traas. Science 322, 1650-1655 (2008)

[2] A modeling study on how cell division affects properties of epithelial tissues
under isotropic growth. P. Sahlin, H. Jonsson. PLoS ONE 5(7):e11750 (2010)

[3] Bozorg, B., Krupinski, P. and Jönsson, H. (2014) Stress and Strain Provide Positional
and Directional Cues in Development PLoS Comp Biol 10(1): e1003410.
https://doi.org/10.1371/journal.pcbi.1003410
